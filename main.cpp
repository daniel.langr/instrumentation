#include <atomic>
#include <iostream>

#include "instrument.h"

int a = 0;

static Instrument inst("test");

int main()
{
   for (int i = 0; i < 1000000; i++)
   {
      auto guard = inst.begin();
      a += i;
   }
}
