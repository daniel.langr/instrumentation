#ifndef INSTRUMENTATION_H
#define INSTRUMENTATION_H

#include <chrono>
#include <iostream>
#include <string>

class Instrument
{
   unsigned long long counter_ = 0;
   std::string name_;

   class Guard
   {
      using clock_type = std::chrono::high_resolution_clock;
      using resolution = std::chrono::nanoseconds;

      clock_type::time_point start_;

      unsigned long long & counter_;

      template <typename T>
      __attribute__((noinline))
      clock_type::time_point safe_time_point(T& dummy)
      {
         __asm__ __volatile__ ("" : "+r" (dummy));
         return clock_type::now();
      }

   public:
      Guard(unsigned long long & counter) : counter_(counter)
      {
         unsigned long long dummy = 0;
         start_ = safe_time_point(dummy);
      }

      ~Guard()
      {
         unsigned long long dummy = 0;
         auto end = safe_time_point(dummy);

         unsigned long long count =
            std::chrono::duration_cast<resolution>(end - start_).count();

         counter_ += count;
      }

   };

public:
   Instrument(const std::string& name) : name_(name) { }

   ~Instrument()
   {
      std::cerr << "Instrument ["
         << name_
         << "] measured time = "
         << get_ms()
         << " [ms]"
         << std::endl;
   }

   Guard begin()
   {
      return Guard(counter_);
   }

   unsigned long long get_ns() const 
   {
      return counter_;
   }

   double get_us() const
   {
      return (double)counter_ / 1.0e3;
   }

   double get_ms() const
   {
      return (double)counter_ / 1.0e6;
   }
};

#endif
